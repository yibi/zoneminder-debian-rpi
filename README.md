These are files that I have edited in order to compile ZoneMinder 1.29 on Debian (Jessie) for the Raspberry Pi 3.
Do feel free to use the files, but please note that I'm not responsible for anything odd happening to you or your machine. :)

The blog post on compiling the package can be found here - https://wp.me/p1IIKc-QE
